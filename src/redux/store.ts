import {configureStore} from '@reduxjs/toolkit';
import appReducer from '@/redux/slices/appSlice';
import todosReducer from '@/redux/slices/todosSlice';

export const store = configureStore({
  reducer: {
    app: appReducer,
    todos: todosReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
