import {createSelector, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {ITodo, TodoFilterType} from '@/models';
import {RootState} from '@/redux/store';

interface TodosState {
  todos: ITodo[];
  activeFilter: TodoFilterType;
}

const initialState: TodosState = {
  todos: [],
  activeFilter: 'all',
};

export const todosSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<ITodo>) => {
      state.todos = [action.payload, ...state.todos];
    },
    deleteTodo: (state, action: PayloadAction<string>) => {
      state.todos = state.todos.filter(todo => todo.id !== action.payload);
    },
    toggleCompletedTodo: (state, action: PayloadAction<{id: string}>) => {
      state.todos = state.todos.map(todo =>
        todo.id === action.payload.id
          ? {...todo, isCompleted: !todo.isCompleted}
          : todo,
      );
    },
    changeActiveFilter: (state, action: PayloadAction<TodoFilterType>) => {
      state.activeFilter = action.payload;
    },
  },
});

export const {addTodo, deleteTodo, toggleCompletedTodo, changeActiveFilter} =
  todosSlice.actions;

export const getFilteredTodos = createSelector(
  [
    (state: RootState) => state.todos.todos,
    (state: RootState) => state.todos.activeFilter,
  ],
  (todos, activeFilter) => {
    if (activeFilter === 'open') {
      return todos.filter(todo => !todo.isCompleted);
    } else if (activeFilter === 'completed') {
      return todos.filter(todo => todo.isCompleted);
    }

    return todos;
  },
);
export const getTodosCountByFilter = createSelector(
  [(state: RootState) => state.todos.todos],
  todos => {
    const counts: {[k in TodoFilterType]: number} = {
      all: todos.length,
      open: 0,
      completed: 0,
    };
    todos.forEach(todo =>
      todo.isCompleted ? (counts['completed'] += 1) : (counts['open'] += 1),
    );

    return counts;
  },
);

export default todosSlice.reducer;
