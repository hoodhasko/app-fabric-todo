import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export type AppTheme = 'dark' | 'light';
export interface AppState {
  theme: AppTheme;
}

const initialState: AppState = {
  theme: 'light',
};

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    changeTheme: state => {
      state.theme = state.theme === 'light' ? 'dark' : 'light';
    },
  },
});

export const {changeTheme} = appSlice.actions;

export default appSlice.reducer;
