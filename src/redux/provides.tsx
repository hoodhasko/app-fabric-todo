'use client';

import {Provider} from 'react-redux';
import {store} from './store';
import {FC, PropsWithChildren} from 'react';
import {useAppSelector} from '@/redux/hooks';

const ThemeProvider: FC<PropsWithChildren> = ({children}) => {
  const theme = useAppSelector(state => state.app.theme);
  return <div className={theme}>{children}</div>;
};

export function Providers({children}: {children: React.ReactNode}) {
  return (
    <Provider store={store}>
      <ThemeProvider>{children}</ThemeProvider>
    </Provider>
  );
}
