export interface ITodo {
  id: string;
  text: string;
  isCompleted: boolean;
}

export type TodoFilterType = 'all' | 'open' | 'completed';

export interface ITodoFilter {
  label: string;
  value: TodoFilterType;
}
