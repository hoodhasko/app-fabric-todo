'use client';
import React, {FC} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {changeActiveFilter, getTodosCountByFilter} from '@/slices';
import {ITodoFilter, TodoFilterType} from '@/models';

const filters: ITodoFilter[] = [
  {label: 'Все', value: 'all'},
  {label: 'Активные', value: 'open'},
  {label: 'Закрытые', value: 'completed'},
];

export const TodoFilter: FC = () => {
  const activeFilter: TodoFilterType = useAppSelector(
    state => state.todos.activeFilter,
  );
  const todosCounts = useAppSelector(getTodosCountByFilter);
  const dispatch = useAppDispatch();

  const filterHandler = (value: TodoFilterType) => {
    dispatch(changeActiveFilter(value));
  };

  return (
    <div className={'flex w-full justify-between mt-5'}>
      {filters.map(filter => (
        <button
          key={filter.value}
          onClick={() => filterHandler(filter.value)}
          className={'flex items-center'}>
          <p
            className={`font-medium  text-xl 
            ${
              activeFilter === filter.value ? 'text-blue-600' : 'text-gray-400'
            }`}>
            {filter.label}
          </p>
          <span
            className={`text-white text-sm font-medium ml-1 px-2 rounded-full
            ${
              activeFilter === filter.value
                ? 'bg-blue-600 dark:bg-blue-900'
                : 'bg-gray-400'
            }`}>
            {todosCounts[filter.value]}
          </span>
        </button>
      ))}
    </div>
  );
};
