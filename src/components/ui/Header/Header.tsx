import React, {FC} from 'react';
import {ChangeToggleButton} from '@/components';

export const Header: FC = () => {
  return (
    <div className={'flex p-10 items-center dark:bg-gray-800'}>
      <h1 className="flex-1 text-5xl font-bold dark:text-white">
        Список задач
      </h1>
      <ChangeToggleButton />
    </div>
  );
};
