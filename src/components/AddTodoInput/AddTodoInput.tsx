'use client';
import React, {FC, useState} from 'react';
import {HiPlus} from 'react-icons/hi';
import {addTodo} from '@/slices';
import {useAppDispatch} from '@/redux/hooks';

interface AddTodoInputProps {}

export const AddTodoInput: FC<AddTodoInputProps> = ({}) => {
  const [todoText, setTodoText] = useState<string>('');

  const dispatch = useAppDispatch();

  const addTodoHandler = () => {
    if (todoText !== '') {
      dispatch(
        addTodo({
          id: new Date().toISOString(),
          text: todoText,
          isCompleted: false,
        }),
      );
      setTodoText('');
    }
  };

  return (
    <div className="flex items-center border-b border-blue-300 py-2">
      <input
        className="appearance-none bg-transparent border-none w-full text-gray-700 dark:text-white mr-3 px-2 leading-tight focus:outline-none"
        type="text"
        placeholder="Описание задачи"
        value={todoText}
        onChange={event => setTodoText(event.target.value)}
      />
      <button
        onClick={addTodoHandler}
        className={
          'flex items-center bg-blue-300 hover:bg-blue-500 text-white font-medium py-1 px-4 rounded-xl dark:bg-blue-600 dark:hover:bg-blue-400'
        }>
        <HiPlus className={'mr-2'} />
        Добавить
      </button>
    </div>
  );
};
