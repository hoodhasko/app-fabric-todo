'use client';
import React, {FC} from 'react';
import {FaCheckCircle, FaTrash} from 'react-icons/fa';
import {ITodo} from '@/models';
import {useAppDispatch} from '@/redux/hooks';
import {deleteTodo, toggleCompletedTodo} from '@/slices';

interface TodoItemProps {
  todo: ITodo;
}

export const TodoItem: FC<TodoItemProps> = ({todo}) => {
  const dispatch = useAppDispatch();

  const todoItemHandler = () => {
    dispatch(toggleCompletedTodo({id: todo.id}));
  };

  const deleteHandler = () => {
    dispatch(deleteTodo(todo.id));
  };

  return (
    <div className={'flex items-center'}>
      <button
        onClick={todoItemHandler}
        className={'flex items-center  mt-2 w-full'}>
        <div>{todo.isCompleted && <FaCheckCircle size={20} />}</div>
        <p
          className={`text-xl text-left ml-2 break-all
          ${todo.isCompleted ? 'line-through' : ''}`}>
          {todo.text}
        </p>
      </button>
      <button onClick={deleteHandler} className={'hover:text-rose-500'}>
        <FaTrash size={20} />
      </button>
    </div>
  );
};
