'use client';
import React, {FC, PropsWithChildren} from 'react';

interface MainCardProps extends PropsWithChildren {
  title?: string;
}

export const MainCard: FC<MainCardProps> = ({title, children}) => {
  return (
    <div className={'shadow rounded-lg p-5 dark:border dark:shadow-none'}>
      {title && <h3 className={'text-3xl font-semibold'}>{title}</h3>}
      {children}
    </div>
  );
};
