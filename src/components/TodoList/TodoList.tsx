'use client';
import React, {FC} from 'react';
import {ITodo} from '@/models';
import {useAppSelector} from '@/redux/hooks';
import {getFilteredTodos} from '@/slices';
import {TodoItem} from '@/components/TodoItem/TodoItem';
import {TodoFilter} from '@/components/TodoFilter/TodoFilter';
import {AddTodoInput} from '@/components/AddTodoInput/AddTodoInput';

export const TodoList: FC = () => {
  const filteredTodos: ITodo[] = useAppSelector(getFilteredTodos);

  return (
    <div className={'mt-2 flex flex-col w-96'}>
      <AddTodoInput />
      <TodoFilter />
      <div className={'mt-5'}>
        {filteredTodos.length === 0 ? (
          <p className={'text-gray-400 text-xl text-center'}>Нет задач</p>
        ) : (
          filteredTodos.map((todo, index) => (
            <div
              key={todo.id}
              className={index !== filteredTodos.length - 1 ? 'border-b' : ''}>
              <TodoItem todo={todo} />
            </div>
          ))
        )}
      </div>
    </div>
  );
};
