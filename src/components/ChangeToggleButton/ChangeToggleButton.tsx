'use client';
import React, {FC} from 'react';
import {changeTheme} from '@/slices';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {BsMoonStarsFill, BsSun} from 'react-icons/bs';

interface ChangeToggleButtonProps {}

export const ChangeToggleButton: FC<ChangeToggleButtonProps> = ({}) => {
  const dispatch = useAppDispatch();
  const theme = useAppSelector(state => state.app.theme);

  return (
    <button className={'p-0 m-0'} onClick={() => dispatch(changeTheme())}>
      {theme === 'dark' ? (
        <BsMoonStarsFill size={25} className={'text-teal-300'} />
      ) : (
        <BsSun size={30} className={'text-yellow-400'} />
      )}
    </button>
  );
};
