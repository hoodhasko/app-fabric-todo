import {Montserrat} from 'next/font/google';
import {Providers} from '@/redux/provides';
import './globals.css';
import {Metadata} from 'next';
import {Header} from '@/components';

const inter = Montserrat({subsets: ['latin']});

export const metadata: Metadata = {
  title: 'Список задач',
  description: 'Веб-приложение "Список задач" для компании Фабрика Приложений',
};

export default function RootLayout({children}: {children: React.ReactNode}) {
  return (
    <html lang="ru">
      <body className={inter.className}>
        <Providers>
          <Header />
          {children}
        </Providers>
      </body>
    </html>
  );
}
